# Roadmap
1. Milestone 1 - basic usage
    1. [x] `jeeves` can be used from cli to execute a single rule

    predicates should include:
    1. `age`
    2. `size`
    3. `match regex`
    4. `extension`

    actions should include
    1. `delete`
    2. `trash`
    2. `move [to]`
    3. `run [custom_shell_command]`

    general syntax of a query
    ```
    [if <predicate> then] <action>
    ```

    usage should look like
    ```sh
    $ jeeves 'if matches ".*.md" then delete'
    ```
2. `jeeves` cli is extended to enable presets

    [x] presets should facilitate an easy to use utility for every day file management

    for instance
    ```
    $ jeeves preset delete_old
    ```
    should locate a preset file corresponding to `delete_old` and apply its rules

    presets are just text files and can be easily shared and installed

    predicates:
    1. [x] `is [type]` - where type is some common file type: video, music, photo, source code, etc.
    2. [ ] extend `matches` to enable matching ingoring or not ignoring case:
       `matches ... ignoring case`

    actions
    1. [ ] `archive <to> [on conflict {overwrite|skip}]`
    2. [ ] `archive with unique name [in]`
    3. [x] `trash` - will put the file in trash

3. [ ] `jeeves` can be auto-setup to periodically run sets of rules on specific folders

    user story should look like this:
    ```sh
    $ cargo install jeeves
    $ jeeves install # opens a dialog that guides the user through the installation, during the installation the user can choose how often jeeves should run, etc. jeeves should install a cron job automatically to run itself periodically
    $ # ...
    $ jeeves here --preset downloads # adds the current directory to jeeves registry, now the preset `downloads` will be run when jeeves runs
    ```