use peg::parser;

use super::SerializableRegex;
use serde::Deserialize;
use std::error::Error;
use std::str::{self, FromStr};
use std::time::Duration;

#[derive(Debug, Deserialize, PartialEq)]
pub enum Predicate {
    And {
        lhs: Box<Predicate>,
        rhs: Box<Predicate>,
    },
    LargerThan(FileSize),
    AgeCompare(FileAgeReference, FileAge),
    Extension(String),
    Matches(SerializableRegex),
    IsCategory(String),
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct FileSize(usize);

impl FileSize {
    pub const fn bytes(n: usize) -> Self {
        Self(n)
    }

    pub const fn kilobytes(n: usize) -> Self {
        Self::bytes(n * 1024)
    }

    pub const fn megabytes(n: usize) -> Self {
        Self::kilobytes(n * 1024)
    }

    pub const fn gigabytes(n: usize) -> Self {
        Self::megabytes(n * 1024)
    }

    pub fn as_bytes(&self) -> usize {
        self.0
    }
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct FileAge(Duration);

#[derive(Debug, Deserialize, PartialEq)]
pub enum FileAgeReference {
    Created,
    Modified,
    Accessed,
}

impl AsRef<Duration> for FileAge {
    fn as_ref(&self) -> &Duration {
        &self.0
    }
}

impl FileAge {
    pub fn seconds(s: u64) -> Self {
        Self(Duration::from_secs(s))
    }

    pub fn days(d: u64) -> Self {
        Self(Duration::from_secs(d * 24 * 60 * 60))
    }
}

parser! {
    grammar predicate() for str {
        rule file_size_quantif() -> u64
            = "B" { 1 }
            / "KB" { 2u64.pow(10) }
            / "MB" { 2u64.pow(10 * 2) }
            / "GB" { 2u64.pow(10 * 3) }

        rule ext() -> String
            = "\"" s:$(['a'..='z'|'A'..='Z'|'.']+) "\"" { String::from(s) }

        rule extension() -> Predicate
            = "has extension " ext:ext() { Predicate::Extension(ext) }

        rule file_size() -> FileSize
            = n:u64() " " q:file_size_quantif() { FileSize((n * q) as usize) }

        rule larger_than() -> Predicate
            = "is larger than " f:file_size() { Predicate::LargerThan(f) }

        rule duration_quantif() -> u64
            = ("seconds" / "second" / "secs" / "sec" / "s") { 1 }
            / ("minutes" / "minute" / "min" / "m") { 60 }
            / ("hours" / "hour" / "h") { 60 * 60 }
            / ("days" / "day" / "d") { 24 * 60 * 60 }
            / ("weeks" / "week" / "w") { 7 * 24 * 60 * 60 }

        rule u64() -> u64
            = n:$(['0'..='9']+) { n.parse().unwrap() }

        rule file_age() -> FileAge
            = n:u64() " " q:duration_quantif() { FileAge(Duration::from_secs(n * q)) }

        rule file_age_reference() -> FileAgeReference
            = "created" { FileAgeReference::Created }
            / "accessed" { FileAgeReference::Accessed }
            / "modified" { FileAgeReference::Modified }

        rule file_age_comparison() -> Predicate
            = "was " r:file_age_reference() " " age:file_age() " ago" { Predicate::AgeCompare(r, age) }

        rule regex() -> SerializableRegex
            = "\"" s:$(['0'..='9'|'a'..='z'|'A'..='Z'|' '..='!'|'#'..='@']+) "\"" { s.parse().unwrap() }

        rule matches() -> Predicate
            = "matches " r:regex() { Predicate::Matches(r) }

        rule category() -> String
            = "\"" s:$(['a'..='z'|'A'..='Z']+) "\"" { s.to_string() }

        rule is_category() -> Predicate
            = "is " c:category() { Predicate::IsCategory(c) }

        pub rule predicate() -> Predicate
            = larger_than() / file_age_comparison() / matches() / extension() / is_category()

        pub rule predicate_rule() -> Predicate
            = p:predicate() ";" { p }
    }
}

impl Predicate {
    fn parse_from_fragments(fragments: &[&str]) -> Result<Self, Box<dyn Error>> {
        if fragments.len() >= 2 {
            let lhs = Predicate::from_str(fragments[0])?;
            let rhs = Predicate::parse_from_fragments(&fragments[1..])?;

            Ok(Predicate::And {
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            })
        } else if fragments.len() == 1 {
            Ok(predicate::predicate(&fragments[0])?)
        } else {
            Err("Cannot parse an empty predicate").map_err(Into::into)
        }
    }
}

impl str::FromStr for Predicate {
    type Err = Box<dyn Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let fragments: Vec<&str> = s.split(" and ").collect();

        Ok(Predicate::parse_from_fragments(&fragments)?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test_case::test_case;

    #[test]
    fn can_parse_predicates() {
        let basic_predicate =
            "matches \".*.md\" and is larger than 20 GB and was created 2 days ago";

        assert_eq!(
            basic_predicate.parse::<Predicate>().unwrap(),
            Predicate::And {
                lhs: Box::new(Predicate::Matches(
                    SerializableRegex::from_str(".*.md").unwrap()
                )),
                rhs: Box::new(Predicate::And {
                    lhs: Box::new(Predicate::LargerThan(FileSize(20 * 2usize.pow(10 * 3)))),
                    rhs: Box::new(Predicate::AgeCompare(
                        FileAgeReference::Created,
                        FileAge(Duration::from_secs(60 * 60 * 24 * 2))
                    )),
                })
            }
        );
    }

    #[test_case("pdf")]
    #[test_case("jpg")]
    #[test_case("docx")]
    fn can_parse_extension_predicates(ext: &str) {
        let has_ext_predicate = format!("has extension \"{}\"", ext);

        assert_eq!(
            has_ext_predicate.parse::<Predicate>().unwrap(),
            Predicate::Extension(ext.to_string())
        );
    }

    #[test_case("20 GB", 20 * 1024 * 1024 * 1024)]
    #[test_case("2 B", 2)]
    #[test_case("3 MB", 3 * 1024 * 1024)]
    #[test_case("20 KB", 20 * 1024)]
    fn can_parse_is_larger_predicate(s: &str, n: usize) {
        let is_larger_predicate = format!("is larger than {}", s);

        assert_eq!(
            is_larger_predicate.parse::<Predicate>().unwrap(),
            Predicate::LargerThan(FileSize(n))
        );
    }

    #[test_case("2 days", 2 * 24 * 60 * 60)]
    #[test_case("3 days", 3 * 24 * 60 * 60)]
    #[test_case("24 s", 24)]
    #[test_case("3 m", 3 * 60)]
    fn can_parse_is_older_than_predicate(s: &str, n: u64) {
        let is_older_predicate = format!("was created {} ago", s);

        assert_eq!(
            is_older_predicate.parse::<Predicate>().unwrap(),
            Predicate::AgeCompare(FileAgeReference::Created, FileAge(Duration::from_secs(n)))
        );
    }

    #[test]
    fn can_parse_matches_predicate() {
        let matches_predicate = "matches \".*\"";
        let expected_regex = SerializableRegex::from_str(".*").unwrap();

        assert_eq!(
            matches_predicate.parse::<Predicate>().unwrap(),
            Predicate::Matches(expected_regex),
        )
    }
}
