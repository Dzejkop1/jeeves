use super::{Action, Predicate};
use std::error::Error;
use std::str;

#[derive(Debug, PartialEq)]
pub struct Rule {
    condition: Option<Predicate>,
    action: Action,
}

impl Rule {
    pub fn condition(&self) -> Option<&Predicate> {
        self.condition.as_ref()
    }

    pub fn action(&self) -> &Action {
        &self.action
    }
}

impl str::FromStr for Rule {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("if") {
            let elems: Vec<&str> = s[3..].splitn(2, "then").collect();

            if elems.len() != 2 {
                return Err(String::from(
                    "A rule must have either a predicate and an action or just an action",
                ))
                .map_err(Into::into);
            }

            let predicate = &elems[0].trim();
            let action = &elems[1].trim();

            Ok(Rule {
                condition: Some(predicate.parse()?),
                action: action.parse()?,
            })
        } else {
            Ok(Rule {
                condition: None,
                action: s.parse()?,
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::rules::{FileAge, FileAgeReference, FileSize};

    #[test]
    fn can_parse_rules() {
        assert_eq!(
            "if is larger than 10 KB and was created 10 days ago then delete"
                .parse::<Rule>()
                .unwrap(),
            Rule {
                condition: Some(Predicate::And {
                    lhs: Box::new(Predicate::LargerThan(FileSize::kilobytes(10))),
                    rhs: Box::new(Predicate::AgeCompare(
                        FileAgeReference::Created,
                        FileAge::days(10)
                    )),
                }),
                action: Action::Delete,
            }
        )
    }
}
