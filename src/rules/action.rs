use peg::parser;

use std::error::Error;
use std::path::PathBuf;
use std::str;

#[derive(Debug, PartialEq)]
pub enum Action {
    Delete,
    Trash,
    Move {
        to: PathBuf,
    },
    Execute {
        cmd: String,
        print_output: bool,
    },
    /// For debugging purposes only
    Print,
}

parser! {
  grammar actions() for str {
    rule cmd() -> String
        = "\"" s:$(['0'..='9'|'a'..='z'|'A'..='Z'|' '..='!'|'#'..='@']+) "\"" { s.to_string() }

    rule path_string() -> String
        = x:$(['a'..='z'|'A'..='Z'|'_'|'-'|'/'|'\\'|' '|'.']+) { String::from(x) }

    rule path() -> PathBuf
        = x:path_string() { PathBuf::from(x) }

    rule exec() -> Action
        = "exec " cmd:cmd() " with output" { Action::Execute { cmd, print_output: true } }
        / "exec " cmd:cmd() { Action::Execute { cmd, print_output: false } }

    rule delete() -> Action
        = "delete" { Action::Delete }

    rule trash() -> Action
        = "trash" { Action::Trash }

    rule move_to() -> Action
        = "move" " " p:path() { Action::Move { to: p } }

    rule print() -> Action
        = "print" { Action::Print }

    pub rule action() -> Action
      = move_to() / delete() / exec() / print() / trash()
  }
}

impl str::FromStr for Action {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(actions::action(s)?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_parse_delete() {
        assert_eq!("delete".parse::<Action>().unwrap(), Action::Delete,);
    }

    #[test]
    fn can_parse_move_to() {
        assert_eq!(
            "move documents".parse::<Action>().unwrap(),
            Action::Move {
                to: PathBuf::from("documents")
            },
        );
    }

    #[test]
    fn can_parse_exec() {
        assert_eq!(
            "exec \"ls\"".parse::<Action>().unwrap(),
            Action::Execute {
                cmd: "ls".to_string(),
                print_output: false
            }
        );

        assert_eq!(
            "exec \"ls\" with output".parse::<Action>().unwrap(),
            Action::Execute {
                cmd: "ls".to_string(),
                print_output: true
            }
        );
    }
}
