use regex::Regex;
use serde::de::{Deserialize, Deserializer, Visitor};

#[derive(Debug)]
pub struct SerializableRegex(Regex);

impl PartialEq for SerializableRegex {
    fn eq(&self, other: &Self) -> bool {
        self.0.as_str().eq(other.0.as_str())
    }
}

impl std::str::FromStr for SerializableRegex {
    type Err = <Regex as std::str::FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(SerializableRegex(Regex::from_str(s)?))
    }
}

impl AsRef<Regex> for SerializableRegex {
    fn as_ref(&self) -> &Regex {
        &self.0
    }
}

struct SerializableRegexVisitor;
impl<'de> Visitor<'de> for SerializableRegexVisitor {
    type Value = SerializableRegex;

    fn expecting(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "a string containing at least 10 bytes")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(SerializableRegex(s.parse::<Regex>().unwrap()))
    }
}

impl<'de> Deserialize<'de> for SerializableRegex {
    fn deserialize<D>(de: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        de.deserialize_str(SerializableRegexVisitor)
    }
}
