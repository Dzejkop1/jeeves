use jeeves::config::Config;
use jeeves::presets::PresetLibrary;
use jeeves::rule_runner::RuleRunner;
use jeeves::rules::Rule;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
enum PresetOpt {
    /// List installed presets
    #[structopt(alias = "ls")]
    List,
    /// Install a preset file
    #[structopt(alias = "i")]
    Install { new_preset_path: PathBuf },
    /// Delete an installed preset
    #[structopt(alias = "d", alias = "del")]
    Delete { preset_name: String },
    /// Execute a preset
    #[structopt(alias = "exec")]
    #[structopt(alias = "x")]
    Execute { preset_name: String },
}

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
enum Command {
    #[structopt(alias = "R")]
    Rule { rule: Rule },
    #[structopt(alias = "P")]
    Preset(PresetOpt),
}

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
struct Opt {
    #[structopt(short, long)]
    #[structopt(default_value = ".")]
    working_dir: PathBuf,

    #[structopt(short, long)]
    config_location: Option<PathBuf>,

    #[structopt(subcommand)]
    command: Command,
}

fn main() {
    let args = Opt::from_args();

    let dirs = directories::BaseDirs::new().expect("Cannot resolve base dirs");

    let config = Config::try_load_or_create_default(
        &args
            .config_location
            .unwrap_or_else(|| dirs.config_dir().join("jeeves").join("config.json")),
    )
    .expect("Couldn't read config");
    let library = PresetLibrary::new(&config.presets_dir).expect("Cannot create preset library");

    let runner = RuleRunner::new(&args.working_dir, &config);

    match args.command {
        Command::Rule { rule } => {
            runner.run_rule(&rule).expect("Couldn't execute rule");
        }
        Command::Preset(preset_command) => match preset_command {
            PresetOpt::Execute { preset_name } => {
                let preset = library.preset(preset_name).unwrap();

                for rule in preset.rules().iter() {
                    runner.run_rule(rule).expect("Couldn't execute rule");
                }
            }
            PresetOpt::List => {
                for preset in library.list().unwrap() {
                    println!("{}", preset);
                }
            }
            PresetOpt::Install { new_preset_path } => {
                library
                    .install(new_preset_path)
                    .expect("Failed to install preset");
            }
            PresetOpt::Delete { preset_name } => {
                library
                    .delete(preset_name)
                    .expect("Failed to remove preset");
            }
        },
    }
}
