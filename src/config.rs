use crate::fs_ext;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::path::{Path, PathBuf};

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    pub presets_dir: PathBuf,
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub categories: HashMap<String, Vec<String>>,
}

fn default_categories() -> HashMap<String, Vec<String>> {
    let mut categories = HashMap::new();

    categories.insert(
        String::from("document"),
        vec![
            String::from("pdf"),
            String::from("docx"),
            String::from("doc"),
            String::from("md"),
        ],
    );

    categories.insert(
        String::from("video"),
        vec![String::from("mp4"), String::from("mkv")],
    );

    categories.insert(
        String::from("audio"),
        vec![
            String::from("mp3"),
            String::from("wav"),
            String::from("flac"),
        ],
    );

    categories.insert(
        String::from("image"),
        vec![
            String::from("png"),
            String::from("jpg"),
            String::from("gif"),
            String::from("psd"),
        ],
    );

    categories
}

impl Config {
    pub fn load(path: impl AsRef<Path>) -> Result<Self, Box<dyn Error>> {
        let mut file = std::fs::File::open(path)?;

        let config = serde_json::from_reader(&mut file)?;

        Ok(config)
    }

    pub fn try_load_or_create_default(path: impl AsRef<Path>) -> Result<Self, Box<dyn Error>> {
        if !path.as_ref().exists() {
            let new_config = Config {
                presets_dir: path.as_ref().parent().ok_or("")?.join("presets"),
                categories: default_categories(),
            };

            let mut file = fs_ext::create_file_and_parent_dirs(&path)?;

            serde_json::to_writer_pretty(&mut file, &new_config)?;

            Ok(new_config)
        } else {
            Self::load(path)
        }
    }
}
