mod library;
mod preset;

pub use library::PresetLibrary;
pub use preset::Preset;

#[derive(Debug, Clone)]
pub struct PresetName(String);
