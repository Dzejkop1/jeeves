use super::Preset;
use std::error::Error;
use std::path::{Path, PathBuf};

pub struct PresetLibrary {
    preset_dir: PathBuf,
}

impl PresetLibrary {
    pub fn new(preset_dir: impl AsRef<Path>) -> Result<Self, Box<dyn Error>> {
        let preset_dir = preset_dir.as_ref();

        if !preset_dir.exists() {
            std::fs::create_dir_all(preset_dir)?;
        }

        Ok(Self {
            preset_dir: preset_dir.to_path_buf(),
        })
    }

    pub fn install(&self, new_preset_path: impl AsRef<Path>) -> Result<(), Box<dyn Error>> {
        Preset::try_open(&new_preset_path)?;

        if let Some(file_name) = new_preset_path.as_ref().file_name() {
            let path_in_preset_dir = self.preset_dir.join(file_name);

            std::fs::copy(new_preset_path, path_in_preset_dir)?;
        }

        Ok(())
    }

    pub fn delete(&self, preset_name: String) -> Result<(), Box<dyn Error>> {
        let preset_path = self.preset_dir.join(preset_name);

        std::fs::remove_file(preset_path)?;

        Ok(())
    }

    pub fn preset(&self, name: impl AsRef<str>) -> Result<Preset, Box<dyn Error>> {
        for entry in self.preset_dir.read_dir()? {
            let entry = entry?;

            if let Some(file_name) = entry.file_name().to_str() {
                if file_name == name.as_ref() {
                    return Preset::try_open(self.preset_dir.join(file_name));
                }
            }
        }

        Err(format!(
            "Cannot find the preset with name {}",
            name.as_ref()
        ))
        .map_err(Into::into)
    }

    pub fn list(&self) -> Result<Vec<String>, Box<dyn Error>> {
        let mut presets = vec![];
        for entry in self.preset_dir.read_dir()? {
            let entry = entry?;

            if let Some(file_name) = entry
                .path()
                .file_stem()
                .and_then(|file_stem| file_stem.to_str())
            {
                presets.push(file_name.to_string());
            }
        }

        Ok(presets)
    }
}
