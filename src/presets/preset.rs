use crate::rules::Rule;
use std::error::Error;
use std::fs;
use std::io::Read;
use std::path::Path;
use std::str;

pub struct Preset {
    rules: Vec<Rule>,
}

impl Preset {
    pub fn rules(&self) -> &[Rule] {
        &self.rules
    }

    pub fn try_open(path: impl AsRef<Path>) -> Result<Self, Box<dyn Error>> {
        if !path.as_ref().exists() {
            return Err(format!("Cannot find preset at path {:?}", path.as_ref()))
                .map_err(Into::into);
        }

        let mut file = fs::File::open(&path)?;
        let mut buffer = String::new();

        file.read_to_string(&mut buffer)?;

        buffer.parse::<Self>()
    }
}

impl str::FromStr for Preset {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let rules = s
            .split(';')
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| s.parse::<Rule>())
            .collect::<Result<Vec<_>, _>>()?;

        Ok(Self { rules })
    }
}
