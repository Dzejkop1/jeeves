use std::fs;
use std::io;
use std::path::Path;

pub fn create_file_and_parent_dirs(path: impl AsRef<Path>) -> io::Result<fs::File> {
    let path = path.as_ref();

    if let Some(parent) = path.parent() {
        fs::create_dir_all(parent)?;
    }

    fs::File::create(path)
}
