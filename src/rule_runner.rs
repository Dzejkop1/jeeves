use crate::config::Config;
use crate::rules::{Action, FileAgeReference, Predicate, Rule};
use std::error::Error;
use std::fs;
use std::path::{Path, PathBuf};
use std::process;

pub struct RuleRunner<'config> {
    working_dir: PathBuf,
    config: &'config Config,
}

impl<'config> RuleRunner<'config> {
    pub fn new(p: impl AsRef<Path>, config: &'config Config) -> Self {
        Self {
            working_dir: p.as_ref().to_owned(),
            config,
        }
    }

    pub fn run_rule(&self, rule: &Rule) -> Result<(), Box<dyn Error>> {
        for entry in self.working_dir.read_dir()? {
            let entry = entry?;

            if !entry.file_type()?.is_file() {
                continue;
            }

            let path = entry.path();

            if self.match_rule(rule, &path)? {
                self.execute_action(rule.action(), &path)?;
            }
        }

        Ok(())
    }

    fn match_rule(&self, rule: &Rule, path: impl AsRef<Path>) -> Result<bool, Box<dyn Error>> {
        if let Some(predicate) = rule.condition() {
            self.match_predicate(predicate, path)
        } else {
            Ok(true)
        }
    }

    fn execute_action(
        &self,
        action: &Action,
        path: impl AsRef<Path>,
    ) -> Result<(), Box<dyn Error>> {
        match action {
            Action::Print => {
                println!("{:?}", path.as_ref());
                Ok(())
            }
            Action::Delete => {
                fs::remove_file(path)?;
                Ok(())
            }
            Action::Trash => {
                trash::remove(path).map_err(|err| format!("Trash error: {:?}", err))?;
                Ok(())
            }
            Action::Move { to } => {
                let dest = self.working_dir.join(to);
                fs::create_dir_all(&dest)?;

                let file_name = path
                    .as_ref()
                    .file_name()
                    .ok_or_else(|| String::from("No file name"))?;

                let dest = dest.join(file_name);

                fs::copy(&path, dest)?;
                fs::remove_file(&path)?;

                Ok(())
            }
            Action::Execute { cmd, print_output } => {
                let file_path = path
                    .as_ref()
                    .to_str()
                    .ok_or_else(|| String::from("Cannot extract file path"))?;

                let cmd = cmd.replace("%f", file_path);

                let cmd_parts: Vec<&str> = cmd.split(' ').collect();

                let cmd = &cmd_parts[0];
                let args = &cmd_parts[1..];

                let output = process::Command::new(cmd).args(args).output()?;

                if *print_output {
                    println!("{}", String::from_utf8(output.stdout)?);
                }

                Ok(())
            }
        }
    }

    fn get_path_extension(path: impl AsRef<Path>) -> Option<String> {
        path.as_ref()
            .extension()
            .and_then(|os_ext| os_ext.to_str())
            .map(|s| s.to_string())
    }

    fn match_predicate(
        &self,
        predicate: &Predicate,
        path: impl AsRef<Path>,
    ) -> Result<bool, Box<dyn Error>> {
        match predicate {
            Predicate::Extension(ext) => Ok(Self::get_path_extension(&path)
                .map(|actual_ext| &actual_ext == ext)
                .unwrap_or(false)),
            Predicate::LargerThan(file_size) => {
                Ok(path.as_ref().metadata()?.len() > file_size.as_bytes() as u64)
            }
            Predicate::AgeCompare(reference_point, file_age) => {
                let metadata = path.as_ref().metadata()?;

                let age = match reference_point {
                    FileAgeReference::Accessed => metadata.accessed()?,
                    FileAgeReference::Created => metadata.created()?,
                    FileAgeReference::Modified => metadata.modified()?,
                }
                .elapsed()?;

                Ok(age > *file_age.as_ref())
            }
            Predicate::Matches(regex) => {
                if let Some(file_name) = path.as_ref().file_name().and_then(|f| f.to_str()) {
                    Ok(regex.as_ref().is_match(file_name))
                } else {
                    Ok(false)
                }
            }
            Predicate::And { lhs, rhs } => {
                let path = path.as_ref();
                Ok(self.match_predicate(lhs.as_ref(), path)?
                    && self.match_predicate(rhs.as_ref(), path)?)
            }
            Predicate::IsCategory(category) => {
                if let Some(ext) = Self::get_path_extension(path) {
                    Ok(self
                        .config
                        .categories
                        .get(category)
                        .map(|extensions| extensions.contains(&ext))
                        .unwrap_or(false))
                } else {
                    Ok(false)
                }
            }
        }
    }
}
